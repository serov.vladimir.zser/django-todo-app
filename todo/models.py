from django.db import models
from django.conf import settings

class Choice(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)
    description = models.CharField(max_length=200)
    is_Complited = models.BooleanField(default=False)